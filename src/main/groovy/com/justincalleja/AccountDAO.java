package com.justincalleja;

import java.math.BigDecimal;
import java.util.List;

public interface AccountDAO {
    int createAccount(BigDecimal initialBalance);
    com.justincalleja.Account findAccountById(int id);
    List<Account> findAllAccounts();
    void updateAccount(Account account);
    void deleteAccount(int id);
}
