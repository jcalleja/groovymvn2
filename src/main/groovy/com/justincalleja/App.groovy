package com.justincalleja

import org.springframework.context.ApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext

class App {

	static main(args) {
		println 'Hello Groovy and Maven in Eclipse'
		
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/applicationContext.xml");
		
		AccountDAO dao = (AccountDAO) applicationContext.getBean("jdbcAccountDAO");
		
		println dao.getClass().getCanonicalName();
		List<Account> accounts = dao.findAllAccounts();
		
		println accounts;

	}
}
